﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ID3TagsFixer.Converters
{
	public class NullOrEmptyToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
				return Visibility.Hidden;

			if (value is string str)
			{
				return string.IsNullOrEmpty(str) ? Visibility.Hidden : Visibility.Visible;
			}

			if (value is IList list)
			{
				return list.Count > 0 ? Visibility.Visible : Visibility.Hidden;
			}

			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
