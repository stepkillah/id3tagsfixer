﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ID3TagsFixer.Services;
using ID3TagsFixer.Views;
using Prism.Ioc;
using Prism.Unity;
using Unity.Lifetime;

namespace ID3TagsFixer
{
	/// <inheritdoc />
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App 
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			
		}

		

		protected override void RegisterTypes(IContainerRegistry containerRegistry)
		{
			containerRegistry.Register<ITagService,TagService>();
			containerRegistry.RegisterForNavigation<MusicListView>();
			
		}

		protected override Window CreateShell()
		{
			return Container.Resolve<MainWindow>();
		}
	}

	
}
