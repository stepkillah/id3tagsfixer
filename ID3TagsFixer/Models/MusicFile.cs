﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;
using TagLib;

namespace ID3TagsFixer.Models
{
	public class MusicFile : BindableBase
	{

		public MusicFile(string path)
		{
			if(string.IsNullOrEmpty(path))
				return;
			Path = path;
			var fileInfo = new FileInfo(path);
			FileSize = $"{ConvertBytesToMegabytes(fileInfo.Length):0.##} MB";
		}

		public string Path { get; set; }
		public string Name => System.IO.Path.GetFileName(Path);
		public string FileSize { get;  }


		static double ConvertBytesToMegabytes(long bytes)
		{
			return (bytes / 1024f) / 1024f;
		}

	}
}
