﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ID3TagsFixer.Helpers;
using ID3TagsFixer.SimpleHelpers;
using TagLib;

namespace ID3TagsFixer.Services
{
	public class TagService : ITagService
	{

		public bool CheckAndFixEncoding(string path)
		{
			if (string.IsNullOrEmpty(path))
				return false;

			try
			{
				TagLib.File file = TagLib.File.Create(path);
				file.Tag.Title = Fix(file.Tag.Title);
				file.Tag.Album = Fix(file.Tag.Album);
				file.Tag.AlbumSort = Fix(file.Tag.AlbumSort);
				file.Tag.Comment = Fix(file.Tag.Comment);
				file.Tag.Copyright = Fix(file.Tag.Copyright);
				file.Tag.AlbumArtists = Fix(file.Tag.AlbumArtists);
				file.Tag.AlbumArtistsSort = Fix(file.Tag.AlbumArtistsSort);
				file.Tag.Composers = Fix(file.Tag.Composers);
				file.Tag.ComposersSort = Fix(file.Tag.ComposersSort);
				file.Tag.Conductor = Fix(file.Tag.Conductor);
				file.Tag.Genres = Fix(file.Tag.Genres);
				file.Tag.Lyrics = Fix(file.Tag.Lyrics);
				file.Tag.Performers = Fix(file.Tag.Performers);
				file.Tag.PerformersSort = Fix(file.Tag.PerformersSort);
				file.Tag.TitleSort = Fix(file.Tag.TitleSort);
				file.Save();
			}
			catch (Exception ex)
			{
				return false;
			}
			return true;
		}
	
		private string Fix(string str)
		{
			if (string.IsNullOrEmpty(str))
				return str;
			return new string(str.ToCharArray().
				Select(x => ((x + 848) >= 'А' && (x + 848) <= 'ё') ? (char)(x + 848) : x).
				ToArray());
		}

		private string[] Fix(string[] str)
		{
			str = str.Select(Fix).ToArray();
			return str;
		}


	}
	
}
