﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TagLib;

namespace ID3TagsFixer
{
	public interface ITagService
	{
		bool CheckAndFixEncoding(string path);
	}
}
