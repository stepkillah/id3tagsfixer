﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ID3TagsFixer.Views;
using Prism.Mvvm;
using Prism.Regions;

namespace ID3TagsFixer.ViewModels
{
	public class MainWindowViewModel : BindableBase
	{
		private readonly IRegionManager _regionManager;

		public MainWindowViewModel(IRegionManager regionManager)
		{
			_regionManager = regionManager;
			_regionManager.RegisterViewWithRegion("MainRegion", typeof(MusicListView));
		}
	}
}
