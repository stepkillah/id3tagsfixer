﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using ID3TagsFixer.Helpers;
using ID3TagsFixer.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using TagLib;

namespace ID3TagsFixer.ViewModels
{
	public class MusicListViewModel : BindableBase, INavigationAware
	{
		private List<MusicFile> _music;
		private string _selectedItem;
		private bool _isLoading;
		private readonly ITagService _tagService;

		public MusicListViewModel(ITagService tagService)
		{
			_tagService = tagService;
		}


		public string SelectedItem
		{
			get => _selectedItem;
			set
			{
				SetProperty(ref _selectedItem, value);
				if (!string.IsNullOrEmpty(_selectedItem))
				{
					_tagService.CheckAndFixEncoding(_selectedItem);
					SelectedItem = null;
				}
			}
		}

		public bool IsLoading
		{
			get => _isLoading;
			set => SetProperty(ref _isLoading, value);
		}


		public List<MusicFile> Music
		{
			get => _music;
			set => SetProperty(ref _music, value);
		}
		public ICommand FixTagsCommand => new DelegateCommand(OnFixTags);

		private void OnFixTags()
		{
			if (Music != null && Music.Any())
			{
				Task.Factory.StartNew(() =>
				{
					try
					{
						IsLoading = true;
						foreach (MusicFile track in Music)
						{
							_tagService.CheckAndFixEncoding(track.Path);
						}

					}
					finally
					{
						IsLoading = false;
					}
				});

			}
		}

		public ICommand SelectFolderCommand => new DelegateCommand(OnSelectFolder);

		private void OnSelectFolder()
		{
			using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
			{
				System.Windows.Forms.DialogResult result = dialog.ShowDialog();
				if (result == DialogResult.OK)
				{
					var folderPath = dialog.SelectedPath;
					if (!Directory.Exists(folderPath))
						Directory.CreateDirectory(folderPath);

					string[] mp3s = Directory.GetFileSystemEntries(
						folderPath, "*", SearchOption.AllDirectories).Where(x=>x.EndsWith(".mp3")).ToArray();
					if (mp3s.Any())
					{
						Music = mp3s.Select(x=> new MusicFile(x)).ToList();
						
					}
				}
			}
		}


		public void OnNavigatedTo(NavigationContext navigationContext)
		{
		}

		public bool IsNavigationTarget(NavigationContext navigationContext)
		{
			return true;
		}

		public void OnNavigatedFrom(NavigationContext navigationContext)
		{
		}
	}
}
